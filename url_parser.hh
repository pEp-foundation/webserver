#pragma once

#include <string>

namespace pEp
{

struct URL
{
	std::string schema; // http/https. parse_url() returns it always in lowercase
	std::string username;
	std::string password;
	std::string host;
	unsigned    port = 0; // port 0 means: default port: 80 for HTTP, 443 for HTTPS
	std::string path_and_query = "/";  // no need to split them down anymore
	std::string fragment;
};

URL parse_url(const char* begin, const char* end);

} // end of namespace pEp
